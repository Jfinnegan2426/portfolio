﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Capstone.Web.Models;
using System.Configuration;

namespace Capstone.Web.DAL
{
    public class ParkSqlDAL
    {
        private const string SQL_GetParks = @"SELECT * FROM park";
        string SQL_FavoriteParks = @"SELECT COUNT(survey_result.parkCode) AS popCount, survey_result.parkCode, park.parkName FROM survey_result JOIN park ON park.parkCode = survey_result.parkCode GROUP BY survey_result.parkCode, park.parkName ORDER BY popCount DESC";

        private string connectionString = ConfigurationManager.ConnectionStrings["ParkWeatherDB"].ConnectionString;

        public List<ParkModel> GetParks()
        {
            List<ParkModel> parks = new List<ParkModel>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_GetParks, conn);

                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        ParkModel p = new ParkModel();
                        p.ParkCode = Convert.ToString(r["parkCode"]);
                        p.ParkName = Convert.ToString(r["parkName"]);
                        p.State = Convert.ToString(r["state"]);
                        p.Acres = Convert.ToInt32(r["acreage"]);
                        p.Elevation = Convert.ToInt32(r["elevationInFeet"]);
                        p.MilesOfTrail = Convert.ToInt32(r["milesOfTrail"]);
                        p.NumberOfSites = Convert.ToInt32(r["numberOfCampsites"]);
                        p.Climate = Convert.ToString(r["climate"]);
                        p.YearFounded = Convert.ToInt32(r["yearFounded"]);
                        p.AnnualVisitors = Convert.ToInt32(r["annualVisitorCount"]);
                        p.Quote = Convert.ToString(r["inspirationalQuote"]);
                        p.QuoteSource = Convert.ToString(r["inspirationalQuoteSource"]);
                        p.ParkDescription = Convert.ToString(r["parkDescription"]);
                        p.EntryFee = Convert.ToInt32(r["entryFee"]);
                        p.AnimalSpecies = Convert.ToInt32(r["numberOfAnimalSpecies"]);

                        parks.Add(p);
                    }
                }
            }
            catch { throw; }
            return parks;
        }

        public ParkModel GetPark(string parkCode)
        {
            string SQL_GetPark = SQL_GetParks + " WHERE parkCode = @parkCode";

            try
            {
                ParkModel p = new ParkModel();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_GetPark, conn);

                    cmd.Parameters.AddWithValue("@parkCode", parkCode);

                    SqlDataReader r = cmd.ExecuteReader();

                    while (r.Read())
                    {
                        p.ParkCode = Convert.ToString(r["parkCode"]);
                        p.ParkName = Convert.ToString(r["parkName"]);
                        p.State = Convert.ToString(r["state"]);
                        p.Acres = Convert.ToInt32(r["acreage"]);
                        p.Elevation = Convert.ToInt32(r["elevationInFeet"]);
                        p.MilesOfTrail = Convert.ToInt32(r["milesOfTrail"]);
                        p.NumberOfSites = Convert.ToInt32(r["numberOfCampsites"]);
                        p.Climate = Convert.ToString(r["climate"]);
                        p.YearFounded = Convert.ToInt32(r["yearFounded"]);
                        p.AnnualVisitors = Convert.ToInt32(r["annualVisitorCount"]);
                        p.Quote = Convert.ToString(r["inspirationalQuote"]);
                        p.QuoteSource = Convert.ToString(r["inspirationalQuoteSource"]);
                        p.ParkDescription = Convert.ToString(r["parkDescription"]);
                        p.EntryFee = Convert.ToInt32(r["entryFee"]);
                        p.AnimalSpecies = Convert.ToInt32(r["numberOfAnimalSpecies"]);
                    }
                    return p;
                }
            }
            catch { throw; }
        }

        public List<ParkModel> FavoriteParks()
        {
            List<ParkModel> parks = new List<ParkModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_FavoriteParks, conn);

                    SqlDataReader r = cmd.ExecuteReader();
                    while (r.Read())
                    {
                        ParkModel p = new ParkModel();
                        p.ParkCode = Convert.ToString(r["parkCode"]);
                        p.ParkName = Convert.ToString(r["parkName"]);
                        p.FavoriteCount = Convert.ToInt32(r["popCount"]);

                        parks.Add(p);
                    }
                }
            }
            catch { throw; }
            return parks;
        }

    }
}
