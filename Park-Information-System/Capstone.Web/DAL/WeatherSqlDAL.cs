﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Capstone.Web.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace Capstone.Web.DAL
{
    public class WeatherSqlDAL
    {
        string SQL_GetWeather = "SELECT * FROM weather JOIN park ON park.parkCode = weather.parkCode ";/*WHERE weather.parkCode = @parkCode";*/

        private string connectionString = ConfigurationManager.ConnectionStrings["ParkWeatherDB"].ConnectionString;

        public List<WeatherModel> GetWeather(/*string parkCode*/)
        {
            try
            {
                List<WeatherModel> wMod = new List<WeatherModel>();
                

                using (SqlConnection conn = new SqlConnection(connectionString))
                {

                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_GetWeather, conn);

                    //cmd.Parameters.AddWithValue("@parkCode", parkCode);

                    SqlDataReader r = cmd.ExecuteReader();

                    while (r.Read())
                    {
                        WeatherModel w = new WeatherModel();
                        w.ParkCode = Convert.ToString(r["parkCode"]);
                        w.Low = Convert.ToInt32(r["low"]);
                        w.High = Convert.ToInt32(r["high"]);
                        w.Forecast = Convert.ToString(r["forecast"]);


                        wMod.Add(w);
                    }
                    return wMod;
                }
            }
            catch { throw; }
        }
    }
}