﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Capstone.Web.Models;
using System.Configuration; 

namespace Capstone.Web.DAL
{
    public class SurveySqlDAL
    {
        private string SQL_AddSurvey = "INSERT INTO survey_result ([parkCode], [emailAddress], [state], [activityLevel]) VALUES (@parkCode, @emailAddress, @state, @activityLevel);";

        private string connectionString = ConfigurationManager.ConnectionStrings["ParkWeatherDB"].ConnectionString;

        public bool AddSurvey(SurveyModel model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_AddSurvey, conn);

                    cmd.Parameters.AddWithValue("@parkCode", model.ParkCode);
                    cmd.Parameters.AddWithValue("@emailAddress", model.Email);
                    cmd.Parameters.AddWithValue("@state", model.State);
                    cmd.Parameters.AddWithValue("@activityLevel", model.ActivityLevel);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    return rowsAffected > 0;
                }
            }
            catch
            {
                throw;
            }
        }









    }
}