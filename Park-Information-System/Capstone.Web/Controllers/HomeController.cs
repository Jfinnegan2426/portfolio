﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Capstone.Web.DAL;
using Capstone.Web.Models;
using System.Configuration;

namespace Capstone.Web.Controllers
{
    public class HomeController : Controller
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["ParkWeatherDB"].ConnectionString;


        // GET: Home
        public ActionResult Index()
        {
            ParkSqlDAL pDAL = new ParkSqlDAL();
            List<ParkModel> parks = pDAL.GetParks();
            return View("Index", parks);
        }

        public ActionResult Detail(string id)
        {
            ParkSqlDAL pDAL = new ParkSqlDAL();
            ParkModel park = pDAL.GetPark(id);

            if (Session["temp"] == null)
            {
                park.IsFahrenheit = "0";
                Session["temp"] = park.IsFahrenheit;
            }
            else
            {
                park.IsFahrenheit = Session["temp"] as string;
            }
            return View("Detail", park);
        }

        [HttpPost]
        public ActionResult Detail(ParkModel p)
        {
            Session["temp"] = p.IsFahrenheit;

            return RedirectToAction("Detail", p);
        }

        public ActionResult Survey()
        {
            return View("Survey");
        }

        [HttpGet]
        public ActionResult FavoritePark()
        {
            ParkSqlDAL pDAL = new ParkSqlDAL();

            List<ParkModel> parks = pDAL.FavoriteParks();

            return View("FavoritePark", parks);
        }

        [HttpPost]
        public ActionResult FavoritePark(SurveyModel opinion)
        {
            SurveySqlDAL s = new SurveySqlDAL();
            s.AddSurvey(opinion);

            ParkSqlDAL pDAL = new ParkSqlDAL();

            List<ParkModel> parks = pDAL.GetParks();

            return RedirectToAction("FavoritePark");
        }

    }
}