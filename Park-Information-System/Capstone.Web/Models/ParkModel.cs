﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Capstone.Web.DAL;
using System.Configuration;
using System.Web.Mvc;

namespace Capstone.Web.Models
{
    public class ParkModel
    {
        public string ParkCode { get; set; }
        public string ParkName { get; set; }
        public string State { get; set; }
        public int Acres { get; set; }
        public int Elevation { get; set; }
        public int MilesOfTrail { get; set; }
        public int NumberOfSites { get; set; }
        public string Climate { get; set; }
        public int YearFounded { get; set; }
        public int AnnualVisitors { get; set; }
        public string Quote { get; set; }
        public string QuoteSource { get; set; }
        public string ParkDescription { get; set; }
        public int EntryFee { get; set; }
        public int AnimalSpecies { get; set; }
        public int FavoriteCount { get; set; }
        public string IsFahrenheit { get; set; }

        public List<SelectListItem> SelectParkList()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            string connectionString = ConfigurationManager.ConnectionStrings["ParkWeatherDB"].ConnectionString;
            ParkSqlDAL p = new ParkSqlDAL();
            List<ParkModel> parks = p.GetParks();
            foreach (ParkModel park in parks)
            {
                SelectListItem s = new SelectListItem() { Text = park.ParkName, Value = park.ParkCode };
                listItems.Add(s);
            }
            return listItems;
        }
        public static List<SelectListItem> MaybeCelcius()
        {
            List<SelectListItem> FC = new List<SelectListItem>();
            SelectListItem One = new SelectListItem() { Text = "Fahrenheit", Value = "0"};
            SelectListItem Two = new SelectListItem() { Text = "Celcius", Value = "1" };
            FC.Add(One);
            FC.Add(Two);
            return FC;
        }
    }
}