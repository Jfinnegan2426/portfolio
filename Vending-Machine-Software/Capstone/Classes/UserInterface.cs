﻿using System;


namespace Capstone.Classes
{
    public class UserInterface
    {
        VendingMachine vendingMachine;

        public UserInterface(VendingMachine vendingMachine)
        {
            this.vendingMachine = vendingMachine;
            this.vendingMachine.Stock();
        }

        public void RunInterface()
        {
            while (true)
            {
                MainMenu();
                string selection = Console.ReadLine();
                Console.Clear();
                if (selection == "1")
                {
                    DisplayVendingMachineItems();
                }
                else if (selection == "2")
                {
                    DisplayPurchaseMenu();
                }
            }

        }
        public void MainMenu()
        {
            Console.WriteLine("Welcome!");
            Console.WriteLine();
            Console.WriteLine("Please Select one of the Following Options:");
            Console.WriteLine("(1) Display Vending Machine Items");
            Console.WriteLine("(2) Purchase");
            Console.WriteLine();
            Console.Write("Selection: ");
        }

        public void DisplayVendingMachineItems()
        {
            Console.WriteLine("Current Inventory: ");
            Console.WriteLine();
            vendingMachine.DisplayInventoryList();
            Console.WriteLine();
            Console.WriteLine("Press any key to return.");
            Console.ReadLine();
            Console.Clear();
            return;
        }

        public void DisplayPurchaseMenu()
        {
            Console.WriteLine("Purchase Options:");
            Console.WriteLine();
            Console.WriteLine("(1) Feed Money");
            Console.WriteLine("(2) Select Product");
            Console.WriteLine("(3) Finish Transaction");
            Console.WriteLine($"Current Money Provided: {vendingMachine.Money}");
            Console.WriteLine();
            Console.Write("Selection: ");
            string option = Console.ReadLine();
            Console.Clear();
            if (option == "1")
            {
                FeedMoneyMenu();
            }
            else if (option == "2")
            {
                SelectProductMenu();
            }
            else if (option == "3")
            {
                FinishTransaction();
            }
            else DisplayPurchaseMenu();
        }

        public void FeedMoneyMenu()
        {
            Console.WriteLine("Feed Money Menu: ");
            Console.WriteLine();
            string input = "";
            Console.Write("Please Enter an Amount to Add ($1, $5, $10): ");
            input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    vendingMachine.AcceptMoney(1);
                    break;
                case "5":
                    vendingMachine.AcceptMoney(5);
                    break;
                case "10":
                    vendingMachine.AcceptMoney(10);
                    break;
                default:
                    Console.WriteLine("Invalid Input Amount!");
                    System.Threading.Thread.Sleep(1000);
                    Console.Clear();
                    FeedMoneyMenu();
                    break;
            }
            Console.Clear();
            DisplayPurchaseMenu();
        }

        public void SelectProductMenu()
        {
            vendingMachine.DisplayInventoryList();
            Console.WriteLine();
            Console.WriteLine();
            Console.Write("Please Select a Location for the Item You Wish to Purchase: ");
            string location = Console.ReadLine();
            string sound = vendingMachine.SelectInventory(location);
            Console.Clear();
            Console.WriteLine("Consuming Product...");
            Console.WriteLine();
            for (int j = 0; j < 10; j++)
            {
                Console.WriteLine(sound);
                Console.WriteLine();
                System.Threading.Thread.Sleep(500);
            }
            System.Threading.Thread.Sleep(1500);
            Console.Clear();
            DisplayPurchaseMenu();

        }

        public void FinishTransaction()
        {
            Console.Write("Dispensing Change");
            for (int i = 0; i < 3; i++)
            {
                Console.Write(".");
                System.Threading.Thread.Sleep(1000);
            }
            vendingMachine.MakeChange();
            Console.WriteLine();
            Console.WriteLine("Have a Nice Day!");
            System.Threading.Thread.Sleep(2000);
            Console.Clear();
            RunInterface();
        }
    }
}
