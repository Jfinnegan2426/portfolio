﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Capstone.Classes
{
    public class VendingMachine
    {
        private decimal money = 0m;
        List<Item> inventory = new List<Item>();

        public string Money { get { return money.ToString("$##0.00"); } }
        public List<Item> Inventory { get { return inventory; } }

        public void Stock()
        {
            using (StreamReader stream = new StreamReader($"{Environment.CurrentDirectory}\\vendingmachine.csv"))
            {
                while (!stream.EndOfStream)
                {
                    string itemString = stream.ReadLine();
                    string[] attribues = itemString.Split('|');
                    Item item = new Item(attribues[0], attribues[1], attribues[2]);
                    inventory.Add(item);
                }
            }
        }

        public void AcceptMoney(int amount)
        {
            decimal moneyBefore = money;
            money += amount;
            LogEvent("FEED MONEY", moneyBefore);
        }

        public string SelectInventory(string location)
        {
            bool isContained = false;
            Item selectedItem = null;
            foreach (Item item in inventory)
            {
                if (item.Location == location)
                {
                    isContained = true;
                    selectedItem = item;
                }
            }
            if (isContained)
            {
                if (selectedItem.Quantity > 0)
                {
                    return Transaction(selectedItem);
                }
                else return "Selected Item is Out of Stock!";
            }
            else
            {
                return "Selected Index Does Not Exist!";
            }
        }

        public string Transaction(Item item)
        {
            if (money >= item.Price)
            {
                string transactionString = $"{item.ProductName} {item.Location}";
                decimal moneyBefore = money;
                money -= item.Price;
                LogEvent(transactionString, moneyBefore);
                item.Quantity -= 1;
                return item.ProduceSound();
            }
            else return "Insufficient Funds!";
        }

        public void DisplayInventoryList()
        {
            int lineCount = 3;
            Console.WriteLine("Location Marker: Product Name:      Price: Quantity:");
            foreach (Item item in inventory)
            {
                Console.SetCursorPosition(0, lineCount);
                Console.Write(item.Location);
                Console.SetCursorPosition(17, lineCount);
                Console.Write(item.ProductName);
                Console.SetCursorPosition(36, lineCount);
                Console.Write(item.Price.ToString("$##0.00"));
                Console.SetCursorPosition(43, lineCount);
                if (item.Quantity == 0)
                {
                    Console.Write("SOLD OUT");
                }
                else Console.Write(item.Quantity.ToString());
                lineCount++;
            }
        }

        public void MakeChange()
        {
            Console.WriteLine();
            decimal moneyBefore = money;
            int quarters = CountCoin(.25m);
            int dimes = CountCoin(.10m);
            int nickels = CountCoin(.05m);
            money = 0.0m;
            LogEvent("GIVE CHANGE", moneyBefore);
            Console.Write("You receive ");
            if (quarters > 0)
            {
                Console.Write($"{quarters} Quarters ");
            }
            if (dimes > 0)
            {
                Console.Write($"{dimes} Dimes ");
            }
            if (nickels > 0)
            {
                Console.Write($"{nickels} Nickels ");
            }
        }

        public int CountCoin(decimal coinValue)
        {
            int numberOfCoin = 0;
            while (money - coinValue >= 0)
            {
                numberOfCoin++;
                money -= coinValue;
            }
            return numberOfCoin;
        }

        public void LogEvent(string transactionType, decimal moneyBefore)
        {
            string logLine = $"{DateTime.Now.ToShortDateString()} {DateTime.Now.ToLongTimeString()} {transactionType}: {moneyBefore.ToString("$##0.00")}    {Money}";
            using (StreamWriter stream = new StreamWriter($"{Environment.CurrentDirectory}\\transactionlog.txt", true))
            {
                stream.WriteLine(logLine);
            }
        }
    }
}
