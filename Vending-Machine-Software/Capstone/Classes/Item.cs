﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Capstone.Classes
{
    public class Item
    {
        private string productName;
        private string location;
        private decimal price;
        private int quantity;
        public string ProductName { get { return productName; } }
        public string Location { get { return location; } }
        public decimal Price { get { return price; } }
        public int Quantity { get { return quantity; } set { quantity = value; } }

        public Item(string location, string name, string price)
        {
            this.location = location;
            productName = name;
            this.price = decimal.Parse(price);
            quantity = 5;
        }

        public string ProduceSound()
        {
            Dictionary<char, string> sounds = new Dictionary<char, string>() { {'A', "Crunch Crunch, Yum!" },
            { 'B', "Munch Munch, Yum!" }, { 'C', "Glug Glug, Yum!" }, { 'D', "Chew Chew, Yum!" }, };
            return sounds[location.ElementAt(0)];
        }
    }
}
