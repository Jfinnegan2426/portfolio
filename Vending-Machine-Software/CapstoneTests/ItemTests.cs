﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Capstone.Classes;

namespace CapstoneTests
{
    [TestClass]
    public class ItemTests
    {
        [TestMethod]
        public void ConstructorTests()
        {
            Item item = new Item("A1", "Test", "3.50");
            Assert.IsNotNull(item);
            Assert.AreEqual("A1", item.Location);
            Assert.AreEqual("Test", item.ProductName);
            Assert.AreEqual(3.50m, item.Price);
        }
        [TestMethod]
        public void ProduceSoundTests()
        {
            Item item = new Item("A1", "Test", "3.50");
            Item item2 = new Item("B1", "Test", "3.50");
            Item item3 = new Item("C1", "Test", "3.50");
            Item item4 = new Item("D1", "Test", "3.50");
            Assert.AreEqual("Crunch Crunch, Yum!", item.ProduceSound());
            Assert.AreEqual("Munch Munch, Yum!", item2.ProduceSound());
            Assert.AreEqual("Glug Glug, Yum!", item3.ProduceSound());
            Assert.AreEqual("Chew Chew, Yum!", item4.ProduceSound());
        }
    }
}
