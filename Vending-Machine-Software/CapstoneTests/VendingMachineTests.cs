﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Capstone.Classes;

namespace CapstoneTests
{
    [TestClass]
    public class VendingMachineTests
    {
        [TestMethod]
        public void ConstructorTests()
        {
            VendingMachine vendingMachine = new VendingMachine();
            Assert.IsNotNull(vendingMachine);
        }
        [TestMethod]
        public void StockTests()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.Stock();
            Assert.AreEqual(16, vendingMachine.Inventory.Count);
        }
        [TestMethod]
        public void AcceptMoneyTests()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.AcceptMoney(10);
            Assert.AreEqual("$10.00", vendingMachine.Money);
            vendingMachine.AcceptMoney(3);
            Assert.AreEqual("$13.00", vendingMachine.Money);
        }
        [TestMethod]
        public void SelectInventoryTests()
        {
            VendingMachine vendingMachine = new VendingMachine();
            vendingMachine.Stock();
            Assert.AreEqual("Selected Index Does Not Exist!", vendingMachine.SelectInventory("FFFFFF"));
            Assert.AreEqual("Insufficient Funds!", vendingMachine.SelectInventory("A1"));
            vendingMachine.AcceptMoney(10);
            Assert.AreEqual("Chew Chew, Yum!", vendingMachine.SelectInventory("D1"));
        }
    }
}
