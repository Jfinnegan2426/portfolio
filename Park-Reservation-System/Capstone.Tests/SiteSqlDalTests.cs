﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using System.Collections.Generic;
using Capstone.Models;
using System.Transactions;
using Capstone.DAL;


namespace Capstone.Tests
{
    [TestClass]
    public class SiteSqlDalTests
    {
        
        private string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=NationalPark;Integrated Security=True";


        

        [TestMethod]
        public void SearchSiteTest()
        {
            DateTime fromDate = new DateTime(2018, 02, 23);
            DateTime toDate = new DateTime(2018, 02, 25);
            SiteSqlDal siteSql = new SiteSqlDal(connectionString);
            List<Site> sites = siteSql.SearchSite(1, 4, fromDate, toDate);
            Assert.IsNotNull(sites);
            Assert.AreEqual(true, sites.Count > 0);

            List<Site> noSites = siteSql.SearchSite(100, 400, fromDate, toDate);
            Assert.IsNotNull(noSites);
        }
    }
}
