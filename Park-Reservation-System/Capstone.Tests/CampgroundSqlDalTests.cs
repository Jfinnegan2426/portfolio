﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using System.Collections.Generic;
using Capstone.Models;
using System.Transactions;
using Capstone.DAL;

namespace Capstone.Tests
{
    [TestClass]
    public class CampgroundSqlDalTests
    {
        //private TransactionScope tran;
        private string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=NationalPark;Integrated Security=True";
        

       

        [TestMethod]
        public void CampgroundsTest()
        {
            CampgroundSqlDal campgroundSql = new CampgroundSqlDal(connectionString);
            List<Campground> campgrounds = campgroundSql.LoadCampgroundList(3);
            Assert.IsNotNull(campgrounds);
            Assert.AreEqual(true, campgrounds.Count > 0);
        }
    }
}
