﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using System.Collections.Generic;
using Capstone.Models;
using System.Transactions;
using Capstone.DAL;

namespace Capstone.Tests
{
    [TestClass]
    public class ReservationSqlDalTests
    {
        private TransactionScope tran;
        private string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=NationalPark;Integrated Security=True";
        private int reservationId = 0;

        [TestInitialize]
        public void Inititialize()
        {
            tran = new TransactionScope();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd;
                conn.Open();


                

                cmd = new SqlCommand("INSERT INTO reservation (site_id, name, from_date, to_date) " +
            "VALUES (8, 'Grant and John Take On the Wilderness', '2018-02-23', '2018-02-26');", conn);
                cmd.ExecuteNonQuery();

                cmd = new SqlCommand("SELECT TOP 1 reservation_id FROM reservation ORDER BY reservation_id DESC;", conn);
                reservationId = (int)cmd.ExecuteScalar();


            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            tran.Dispose();
        }

        [TestMethod]
        public void CreateReservationTest()
        {
            DateTime fromDate = new DateTime(2018, 02, 23);
            DateTime toDate = new DateTime(2018, 02, 26);
            ReservationSqlDal reservationSql = new ReservationSqlDal(connectionString);
            bool didWork = reservationSql.MakeReservation(8, "Grant and John Take On the Wilderness", fromDate, toDate);

            Assert.AreEqual(true, didWork);
        }
        [TestMethod]
        public void ShowConfirmationCheck()
        {
            ReservationSqlDal reservationSql = new ReservationSqlDal(connectionString);
            int confirmationValue = reservationSql.ShowConfirmation().ReservationId;
            Assert.AreEqual(reservationId, confirmationValue);
        }

    }
}
