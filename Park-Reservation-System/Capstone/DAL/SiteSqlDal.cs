﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Capstone.Models;

namespace Capstone.DAL
{
    public class SiteSqlDal
    {
        private const string SQL_GetAvailableSites = "Select distinct TOP 5 site.*, campground.name, (DATEDIFF(DAY, @fromDate, @toDate) * campground.daily_fee) AS total_cost from site " +
            "join campground on site.campground_id = campground.campground_id " +
            "where site.campground_id = @campgroundId " +
            "and site_id not in " +
            "(SELECT site.site_id FROM site " +
            "JOIN reservation ON site.site_id = reservation.site_id " +
            "WHERE reservation.from_date BETWEEN @fromDate AND @toDate " +
            "OR reservation.to_date BETWEEN @fromDate AND @toDate " +
            "OR (reservation.from_date < @fromDate AND reservation.to_date > @toDate))";

        private string connectionString;


        public SiteSqlDal(string databaseconnectionString)
        {
            connectionString = databaseconnectionString;
        }

        public List<Site> SearchSite(int pickedParkID, int campgroundId, DateTime fromDate, DateTime toDate)
        {
            //Loads a list of sites that do not have conflicting reservation dates.
            List<Site> output = new List<Site>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SQL_GetAvailableSites, conn);
                    cmd.Parameters.AddWithValue("@fromDate", fromDate);
                    cmd.Parameters.AddWithValue("@toDate", toDate);
                    cmd.Parameters.AddWithValue("@campgroundId", campgroundId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Site s = new Site();
                        s.SiteNumber = Convert.ToInt32(reader["site_number"]);
                        s.SiteId = Convert.ToInt32(reader["site_id"]);
                        s.Accessible = Convert.ToBoolean(reader["accessible"]);
                        s.MaxOccupancy = Convert.ToInt32(reader["max_occupancy"]);
                        s.MaxRvLength = Convert.ToInt32(reader["max_rv_length"]);
                        s.Utilities = Convert.ToBoolean(reader["utilities"]);
                        s.Cost = Convert.ToDecimal(reader["total_cost"]);

                        output.Add(s);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return output;
        }

        


    }
}
