﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Capstone.Models;


namespace Capstone.DAL
{
    public class CampgroundSqlDal
    {
        private const string SQL_GetCampgrounds = "SELECT * FROM campground JOIN park ON campground.park_id = park.park_id " +
            "WHERE campground.park_id = @pickedParkId";
        private string connectionString;

        public CampgroundSqlDal(string databaseConnection)
        {
            connectionString = databaseConnection;
        }

        public List<Campground> LoadCampgroundList(int pickedParkId)
        {
            //Loads and holds a specific park's list of campgrounds.
            List<Campground> output = new List<Campground>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_GetCampgrounds, conn);

                    cmd.Parameters.AddWithValue("@pickedParkId", pickedParkId);

                    SqlDataReader reader = cmd.ExecuteReader();

                    

                    while (reader.Read())
                    {
                        Campground c = new Campground();
                        c.CampgroundId = Convert.ToInt32(reader["campground_id"]);
                        c.ParkId = Convert.ToInt32(reader["park_id"]);
                        c.Name = Convert.ToString(reader["name"]);
                        c.OpenFromMonth = Convert.ToString(reader["open_from_mm"]);
                        c.OpentoMonth = Convert.ToString(reader["open_to_mm"]);
                        c.DailyFee = Convert.ToDecimal(reader["daily_fee"]);
                        
                        output.Add(c);

                    }

                }
                
            }
            catch (SqlException ex)
            {
                throw;
            }
            return output;
        }
        
    }
}
