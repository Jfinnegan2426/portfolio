﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Capstone.Models;

namespace Capstone.DAL
{
    public class ParkSqlDal
    {
        private const string SQL_GetParkInfo = "SELECT * FROM park";
        private string connectionString;
        public int PickedParkId { get; set; }

        public ParkSqlDal(string databaseconnectionString)
        {
            connectionString = databaseconnectionString;
        }

        public List<Park> LoadParkInformation()
        {
            //Loads and holds all information from the park table.
            List<Park> output = new List<Park>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_GetParkInfo, conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Park p = new Park();
                        p.ParkId = Convert.ToInt32(reader["park_id"]);
                        p.Name = Convert.ToString(reader["name"]);
                        p.Locations = Convert.ToString(reader["location"]);
                        p.EstablishDate = Convert.ToDateTime(reader["establish_date"]);
                        p.Area = Convert.ToInt32(reader["area"]);
                        p.Visitors = Convert.ToInt32(reader["visitors"]);
                        p.Description = Convert.ToString(reader["description"]);

                        output.Add(p);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return output;
        }
        public Park GetParkInformation(int userInput)
        {
            //Loads the information for the specified park.
            List<Park> parkList = LoadParkInformation();
            PickedParkId = parkList[userInput].ParkId;
            return parkList[userInput];
            


        }
    }
}
