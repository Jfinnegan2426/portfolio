﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Capstone.Models;

namespace Capstone.DAL
{
    public class ReservationSqlDal
    {
        private const string SQL_MakeReservation = "INSERT INTO reservation (site_id, name, from_date, to_date) " +
            "VALUES(@siteId, @name, @fromDate, @toDate)";
        private const string SQL_ShowReservationConfirmation = "SELECT TOP 1 reservation_id FROM reservation ORDER BY reservation_id DESC";
        private string connectionString;

        public ReservationSqlDal(string databaseconnectionString)
        {
            connectionString = databaseconnectionString;
        }

        public bool MakeReservation(int siteId, string name, DateTime fromDate, DateTime toDate)
        {
            //Completes the actual reservation.
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SQL_MakeReservation, conn);

                    cmd.Parameters.AddWithValue("@siteId", siteId);
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.Parameters.AddWithValue("@fromDate", fromDate);
                    cmd.Parameters.AddWithValue("@toDate", toDate);

                    int rowsAffected = cmd.ExecuteNonQuery();

                    return (rowsAffected > 0);
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
        public Reservation ShowConfirmation()
        {
            //lists the reservation confirmation along with the reservation ID
            Reservation r = new Reservation();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SQL_ShowReservationConfirmation, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        
                        r.ReservationId = Convert.ToInt32(reader["reservation_id"]);


                    }
                    
                    

                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return r;
        }
        
    }
}
