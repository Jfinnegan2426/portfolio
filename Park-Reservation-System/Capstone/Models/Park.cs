﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class Park
    {
        public int ParkId { get; set; }
        public string Name { get; set; }
        public string Locations { get; set; }
        public DateTime EstablishDate { get; set; }
        public int Area { get; set; }
        public int Visitors { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return $"{Name}\nLocations: {Locations.PadRight(30)}\nEstablished:" +
                $" {EstablishDate.ToString("d").PadRight(30)}\nArea: {Area.ToString().PadRight(30)}\nAnnual Visitors: {Visitors.ToString().PadRight(30)}\n\n{Description.ToString()}";
        }
    }
}
