﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class Campground
    {
        private string openFromMonth;
        private string opentoMonth;
        public int CampgroundId { get; set; }
        public int ParkId { get; set; }
        public string Name { get; set; }
        public string OpenFromMonth
        {
            get
            {
                return openFromMonth;
            }
            set
            {
                switch (value)
                {
                    case "1":
                        openFromMonth = "January";
                        break;
                    case "2":
                        openFromMonth = "February";
                        break;
                    case "3":
                        openFromMonth = "March";
                        break;
                    case "4":
                        openFromMonth = "April";
                        break;
                    case "5":
                        openFromMonth = "May";
                        break;
                    case "6":
                        openFromMonth = "June";
                        break;
                    case "7":
                        openFromMonth = "July";
                        break;
                    case "8":
                        openFromMonth = "August";
                        break;
                    case "9":
                        openFromMonth = "September";
                        break;
                    case "10":
                        openFromMonth = "October";
                        break;
                    case "11":
                        openFromMonth = "November";
                        break;
                    case "12":
                        openFromMonth = "December";
                        break;

                    default:
                        break;
                }
            }
        }
        public string OpentoMonth
        {
            get
            {
                return opentoMonth;
            }
            set
            {
                switch (value)
                {
                    case "1":
                        opentoMonth = "January";
                        break;
                    case "2":
                        opentoMonth = "February";
                        break;
                    case "3":
                        opentoMonth = "March";
                        break;
                    case "4":
                        opentoMonth = "April";
                        break;
                    case "5":
                        opentoMonth = "May";
                        break;
                    case "6":
                        opentoMonth = "June";
                        break;
                    case "7":
                        opentoMonth = "July";
                        break;
                    case "8":
                        opentoMonth = "August";
                        break;
                    case "9":
                        opentoMonth = "September";
                        break;
                    case "10":
                        opentoMonth = "October";
                        break;
                    case "11":
                        opentoMonth = "November";
                        break;
                    case "12":
                        opentoMonth = "December";
                        break;

                    default:
                        break;
                }
            }
        }
        public decimal DailyFee { get; set; }



        public override string ToString()
        {
            return Name.PadRight(30) + OpenFromMonth.ToString().PadRight(30) + OpentoMonth.ToString().PadRight(30) + "$" + DailyFee.ToString("0.00").PadRight(30);
        }


    }
}
