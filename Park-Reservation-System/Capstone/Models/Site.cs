﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone.Models
{
    public class Site
    {
        public int SiteId { get; set; }
        public int CampgroundId { get; set; }
        public int SiteNumber { get; set; }
        public int MaxOccupancy { get; set; }
        public bool Accessible { get; set; }
        public int MaxRvLength { get; set; }
        public bool Utilities { get; set; }
        public decimal Cost { get; set; }
        

        public override string ToString()
        {
            
            return SiteNumber.ToString().PadRight(15) + MaxOccupancy.ToString().PadRight(15) + Accessible.ToString().PadRight(15) +
                MaxRvLength.ToString().PadRight(15) + Utilities.ToString().PadRight(15) + Cost.ToString("0.00").PadRight(15);
        }

    }
}
