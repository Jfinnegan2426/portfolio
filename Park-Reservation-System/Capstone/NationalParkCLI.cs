﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Capstone.DAL;
using Capstone.Models;
using System.Data.SqlClient;

namespace Capstone
{
    public class NationalParkCLI
    {
        private const string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=NationalPark;Integrated Security=True";
        

        public void RunCli()
        {

            PrintHeader();

            while (true)
            {
                Park currentPark = null;
                List<Campground> campgroundList = null;
                DateTime fromDate = DateTime.Now;
                DateTime toDate = DateTime.Now;
                List<Site> siteList = null;
                bool errorCreated = false; //to track if an exception has been generated through the CLI
                bool returnToMenu = false;

                Console.WriteLine("Main Menu");
                int mainMenuInput = PrintMainMenu();

                if (mainMenuInput == 0) //as an exit mechanism for the program. //FORMERLY KILLSWITCH
                {
                    Console.Write("Have a nice day!");
                    System.Threading.Thread.Sleep(1500);
                    break;
                }
                else if (mainMenuInput == -1)
                {
                    Console.Clear();
                    Console.WriteLine("Something went wrong. Returning to the main menu");
                    errorCreated = true;
                    System.Threading.Thread.Sleep(2000);
                }
                if (!errorCreated)
                {
                    try
                    {
                        currentPark = PrintParkInfo(mainMenuInput);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Invalid selection. Please try again.");
                        errorCreated = true;
                        System.Threading.Thread.Sleep(1500);
                        Console.Clear();
                    }
                }
                if (!errorCreated)
                {
                    campgroundList = PrintParkMenu(mainMenuInput, currentPark);
                    if (campgroundList == null)
                    {
                        returnToMenu = true;
                    }
                }
                if (!errorCreated && !returnToMenu)
                {
                    returnToMenu = PrintCampgroundMenu(campgroundList, currentPark);
                }
                if (!errorCreated && !returnToMenu)
                {
                    try
                    {
                        Console.Write("What is the arrival date (YYYY-MM-DD)? "); //move these up one level.
                        fromDate = DateTime.Parse(Console.ReadLine());
                        Console.Write("What is the departure date (YYYY-MM-DD)? ");
                        toDate = DateTime.Parse(Console.ReadLine());
                        siteList = CampgroundReservationMenu(currentPark.ParkId, fromDate, toDate, campgroundList);
                        if (siteList == null)
                        {
                            returnToMenu = true;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Invalid Selection. Returning to the Main Menu");
                        errorCreated = true;
                        System.Threading.Thread.Sleep(1500);
                        Console.Clear();
                    }
                }
                if (!errorCreated && !returnToMenu)
                {
                    SiteReservationMenu(siteList, fromDate, toDate);
                }

                if(returnToMenu)
                {
                    Console.WriteLine("Returning to the Main Menu");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                }





            }

        }

        public void PrintHeader()
        {
            Console.WriteLine("WELCOME TO THE BEST NATIONAL PARK RESERVATION SITE EVER CREATED");
            Console.WriteLine();

        }
        public int PrintMainMenu()
        {
            ParkSqlDal parkList = new ParkSqlDal(connectionString);
            List<Park> parks = parkList.LoadParkInformation(); //Loads the list of parks
            int mainMenuInput;

            for (int i = 0; i < parks.Count; i++)
            {
                Console.WriteLine($"{i + 1}) {parks[i].Name}");
            }
            Console.WriteLine("Press '0' to quit");
            Console.WriteLine();
            Console.Write("Select the park you are interested in visiting: ");

            try
            {
                mainMenuInput = int.Parse(Console.ReadLine());
                return mainMenuInput;
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid selection");
                Console.WriteLine();
                Console.WriteLine("Please try again!");
                System.Threading.Thread.Sleep(1250);
                Console.Clear();
                return -1;
                
            }
            


        }

        public Park PrintParkInfo(int mainMenuInput)
        {
            //try //to catch any nonexistant selections
            
            string parkName;
            Console.Clear();
            ParkSqlDal parkInfo = new ParkSqlDal(connectionString);
            Park park = parkInfo.GetParkInformation(mainMenuInput - 1); //-1 to put it back into index range.
            parkName = park.Name; 
            Console.WriteLine(park);
            return park;
           

        }
        public List<Campground> PrintParkMenu(int mainMenuInput, Park currentPark)
        {
            string name = "Name";
            string open = "Open";
            string close = "Close";
            string dailyFee = "Daily Fee";
            List<Campground> campgroundList = null;


            Console.WriteLine("\nSelect a Command");
            Console.WriteLine("1) View Campgrounds");
            Console.WriteLine("Press Any Other Key to Return to Previous Screen");
            Console.Write("\nPlease enter your selection here: ");
            string userInput = Console.ReadLine();

            if (userInput == "1")
            {
                CampgroundSqlDal campgroundSql = new CampgroundSqlDal(connectionString);
                campgroundList = campgroundSql.LoadCampgroundList(mainMenuInput); //Loads in list of park campgrounds.

                Console.Clear();
                Console.WriteLine($"\nPark Campgrounds\n{currentPark.Name} National Park:\n");
                Console.WriteLine($"{name.PadRight(30) + open.PadRight(30) + close.PadRight(30) + dailyFee.PadRight(30)}");
                foreach (Campground campground in campgroundList) //to print campground names
                {
                    Console.WriteLine(campground);
                }
            }


            return campgroundList;
            

        }
        




        public bool PrintCampgroundMenu(List<Campground> campgroundList, Park currentPark)
        {
            
            string name = "Name";
            string open = "Open";
            string close = "Close";
            string dailyFee = "Daily Fee";
            //This menu works in conjunction with the PrintParkMenu method to present the user's options.
            Console.WriteLine("Select a Command");
            Console.WriteLine("1) Search for Available Reservations");
            Console.WriteLine("Press Any Other Key to Return to the Main Menu");
            Console.Write("\nPlease enter your selection here: ");
            string userInput = Console.ReadLine();

            if (userInput == "1")
            {


                
                Console.Clear();
                Console.WriteLine($"\nPark Campgrounds\n{currentPark.Name} National Park:\n");
                Console.WriteLine($"   {name.PadRight(30) + open.PadRight(30) + close.PadRight(30) + dailyFee.PadRight(30)}");
                int count = 0;

                foreach (Campground campground in campgroundList)
                {
                    count++;
                    Console.WriteLine($"#{count} {campground}");

                }
                return false;
            }
            else
            {
                return true;
            }


            
            

        }
        public List<Site> CampgroundReservationMenu(int parkId, DateTime fromDate, DateTime toDate, List<Campground> campgroundList) //add from and to date parameters
        {
            string siteNum = "Site Number";
            string maxOcc = "Max Occupancy";
            string accessible = "Accessible?";
            string maxRvLength = "Max RV Length";
            string utility = "Utility";
            string cost = "Cost";
            int campgroundSelection = 0;
            bool errorCreated = false;
            List<Site> siteList = null;
            

            Console.Write("Which campground (enter '0' to cancel)? ");
            try
            {
                campgroundSelection = int.Parse(Console.ReadLine());
                if (campgroundSelection == 0)
                {
                    return siteList;
                }

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Invalid Selection. Returning to the main menu");
                errorCreated = true; //to pull back to the main menu
                System.Threading.Thread.Sleep(1500);
                return siteList;

            }

            if ((campgroundSelection > 0) && (!errorCreated)) //to allow execution of the 0/escape function and deal with noninteger campground selections.
            {
                try //to deal with invalid selections.
                {
                    
                    SiteSqlDal siteSql = new SiteSqlDal(connectionString);

                    siteList = siteSql.SearchSite(parkId, campgroundList[campgroundSelection - 1].CampgroundId, fromDate, toDate);
                    //Loads in a list of sites in the given campground with no reservation at that time.
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid selection. Returning to the main menu");
                    System.Threading.Thread.Sleep(1500);
                    errorCreated = true;
                    siteList = null;
                    return siteList;
                }

                if (!errorCreated) //to return to the main menu if an exception is thrown.
                {
                    if (siteList.Count > 0)
                    {

                        if ((siteList[0].Cost <= 0)) //to deal with negative and 0 day stay lengths.
                        {
                            Console.Clear();
                            Console.WriteLine("Something went wrong with your dates. Returning you to the main menu.");
                            System.Threading.Thread.Sleep(1250);
                            siteList = null;
                            return siteList;
                        }
                        else
                        {
                            if (siteList.Count > 0) //To deal with the possibility of no reservations available.
                            {

                                Console.WriteLine($"\n{siteNum.PadRight(15) + maxOcc.PadRight(15) + accessible.PadRight(15) + maxRvLength.PadRight(15) + utility.PadRight(15) + cost.PadRight(15)}");
                                int count = 0;
                                foreach (Site site in siteList)
                                {
                                    count++;
                                    Console.WriteLine($"#{count} {site}");

                                }
                                
                                return siteList;
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("There are no reservations available at that time. Returning to the main menu");
                                System.Threading.Thread.Sleep(1500);
                                siteList = null;
                                return siteList;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("There are no sites in this campground with availability at that time. Please select another campground.");
                        System.Threading.Thread.Sleep(2000); //to deal with a lack of availability.
                        siteList = null;
                        return siteList;
                    }
                }

            }
            else
            {
                Console.Clear();
                Console.WriteLine("Thank you for visiting the Best National Reservation Site Ever created!");

                System.Threading.Thread.Sleep(1250);
                siteList = null;
                return siteList;
            }
            siteList = null;
            return siteList;

        }
        public void SiteReservationMenu(List<Site> siteList, DateTime fromDate, DateTime toDate)
        {
            Console.Write("\nWhich selection should be reserved (enter '0' to cancel)?: ");
            int selection = 0;
            try
            {
                selection = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                selection = -1; //to deal with invalid selections in the seletion parse and exit the menu
            }
            if ((selection > 0) && (selection <= siteList.Count)) // to stop from asking what the name will be under if invalid. '<' to '<=' to account for lists.Count = 1.

            {
                Console.Write("What name should the reservation be made under?: ");
                string name = Console.ReadLine();
                ReservationSqlDal reservationSql = new ReservationSqlDal(connectionString);
                bool didWork = false;

                try //to deal with invalid selections
                {
                    didWork = reservationSql.MakeReservation(siteList[selection - 1].SiteId, name, fromDate, toDate);
                }
                catch (Exception e)
                {

                }
                Reservation reservation = reservationSql.ShowConfirmation();
                if (didWork)
                {
                    Console.WriteLine("Success!");
                    Console.Write(reservation);
                }

                else
                {
                    Console.WriteLine("Your reservation has failed. Please try again.");
                }
                Console.WriteLine("Returning to the main menu");
                System.Threading.Thread.Sleep(2000);
                Console.WriteLine();
                Console.Clear();

            }
            else if ((selection < 0) || (selection > siteList.Count)) //to deal with invalid selections in the selection parse.
            {
                Console.Clear();
                Console.WriteLine("Invalid selection. Returning to the main menu.");
                System.Threading.Thread.Sleep(1500);
                Console.Clear();
            }

            else
            {
                Console.Clear();
                Console.WriteLine("Thank you for visiting the Best National Reservation Site Ever created!");
                Console.WriteLine("Returning you to the main menu.");
                System.Threading.Thread.Sleep(1250);
                Console.Clear();

            }
        }

    }
}
